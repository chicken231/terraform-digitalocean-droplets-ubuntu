# Terraform for Digital Ocean Droplets

## Purpose

This repository contains **Terraform** code and default variables that allow one to create **Digital Ocean Droplet(s)** with ease.

### About

+ Tested with creating **Ubuntu 18.04** Droplets.
+ All *arguments* for the `digitalocean_droplet` **Terraform** resource are enumerated in `main.tf` and `variables.tf`.
+ `tags` and `volume_ids` are commented out for first-time ease. You can un-comment references to these two variables, but there are limitations with **Terraform** at least when it comes to `tags` (i.e. `tag` must exist before creating a Droplet with that `tag`.).
+ All available *outputs* will be printed upon completion of `terraform apply`.

### How

Provisioners `file` and `remote-exec` are used to:

1. Overwrite `/etc/sysctl.conf` with web-server optimized and kernel-hardened parameters. Beware that these settings can cause some very squirrelly issues with various software. When in doubt with weird issues down the road with one of these droplets, disable the use of this config file.
2. Add a new user according to the `ssh_user` variable.
3. Add this user to the `admin` group.
4. Modify `/etc/sudoers` so that the `admin` group can execute `sudo` without a password.
5. Modify `/etc/ssh/sshd_config` to disallow `root` user login and allow `PubkeyAuthentication`.
6. Move ssh keys from `/root/.ssh/` to `/home/YOUR_SSH_USER/.ssh/`.

### Requirements

1. [Terraform](https://www.terraform.io/downloads.html), obviously.
2. A token for **Digital Ocean** that can `write`.
3. An SSH key registered with **Digital Ocean**.

#### Required Variables

As long as these variables are provided, the defaults are to create one droplet in the `sfo2` region with IPv6, monitoring, and private networking enabled.

1. `do_token`: (string) Digital Ocean API Token.
2. `ssh_user`: (string) Name of user that will be allowed password-less **sudo** and **ssh access**.
3. `ssh_key_path`: (string) path on local machine to SSH private key.
4. `vm_ssh_key_ids`: (list) ID(s) of SSH Key ID's according to Digital Ocean. If providing over CLI, be wary of the data type. For example:

With environment variables:

```bash
export TF_VAR_do_token='DIGITAL_OCEAN_TOKEN_HERE' && \
export TF_VAR_ssh_user='superuser' && \
export TF_VAR_ssh_key_path='/home/superuser/.ssh/id_rsa' && \
export TF_VAR_vm_ssh_key_ids='[12483191]'
```

## Usage

```bash
terraform plan
terraform apply
```

### Useful Environment Variables

Export your [Digital Ocean API key](https://cloud.digitalocean.com/account/api/tokens):
```bash
export DIGITALOCEAN_TOKEN='YOUR_TOKEN_HERE'
```

```bash
export TF_LOG_='DEBUG' && \
export TF_LOG_PATH='tf.log'
```

### Get Droplet Sizes

*Note: Piping to `jq` required only for pretty-printing JSON response*

```bash
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${DIGITALOCEAN_TOKEN}" "https://api.digitalocean.com/v2/sizes?page=1" | jq
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${DIGITALOCEAN_TOKEN}" "https://api.digitalocean.com/v2/sizes?page=2" | jq
```

*NOTE: Multi-page responses*

### Locate your SSH Key ID(s)

If you want SSH keys already associated with your **Digital Ocean** account to be set on the **Droplets**, you first need to get the ID(s) of the key(s) according to **Digital Ocean**.

1. [Use cURL](https://developers.digitalocean.com/documentation/v2/#list-all-keys) to list keys and their IDs:
```bash
curl -H "Content-Type: application/json" -H "Authorization: Bearer ${DIGITALOCEAN_TOKEN}" "https://api.digitalocean.com/v2/account/keys" | jq
```

2. Use the desired **Key ID**  from this output as the value(s) for the `vm_ssh_key_ids` variable.

## References

+ https://www.terraform.io/docs/providers/do/r/droplet.html
+ https://developers.digitalocean.com/documentation/v2/
